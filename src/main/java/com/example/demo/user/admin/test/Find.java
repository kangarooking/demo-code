package com.example.demo.user.admin.test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Find {
    public static void main(String[] args) {
        String sitStr = "IS_HOT,\n" +
                "    SYS_SOU,\n" +
                "    MGR_STAT,\n" +
                "    SALE_STA,\n" +
                "    RECOMM_INDEX,\n" +
                "    INCOME_DIST_WAY,\n" +
                "    CHARGE_CLS,\n" +
                "    CURRENCY,\n" +
                "    AD_FLAG,\n" +
                "    RISK_LVL,\n" +
                "    REVENUE_TYPE,\n" +
                "    DIST_STAT,\n" +
                "    INVEST_HORI,\n" +
                "    IS_GRP,\n" +
                "    IS_FROZEN_FUNDS,\n" +
                "    RES_MODE,\n" +
                "    IS_NEED_RES,\n" +
                "    UPD_TIME,\n" +
                "    ON_SHELVES_DATE,\n" +
                "    PRO_TYPE,\n" +
                "    SECONDARY_SCALE,\n" +
                "    PRIORITY_SCALE,\n" +
                "    INVE_DATE,\n" +
                "    LATEST_SIZE,\n" +
                "    REGI_HOST_DATE,\n" +
                "    MINI_CAPI_INCRE,\n" +
                "    MAX_PART_AMT,\n" +
                "    MIN_PART_AMT,\n" +
                "    PAR_VALUE,\n" +
                "    PART_PRICE,\n" +
                "    AD_AMT,\n" +
                "    FACE_VALUE,\n" +
                "    ISS_AMT,\n" +
                "    ISSUE_MODE,\n" +
                "    REGI_DATE,\n" +
                "    TERM_DATE,\n" +
                "    GRP_DATE,\n" +
                "    ISS_END_DATE,\n" +
                "    ISS_BGN_DATE,\n" +
                "    COMM_ALL,\n" +
                "    COMM_1Y,\n" +
                "    COMM_YEAR,\n" +
                "    INVE_START,\n" +
                "    MAX_RAISE_SIZE,\n" +
                "    MIN_RAISE_SIZE,\n" +
                "    RAISE_ISSU_SIZE,\n" +
                "    DURA_TIME,\n" +
                "    RAISE_TIME,\n" +
                "    RECM_END_DATE,\n" +
                "    RECM_BGN_DATE,\n" +
                "    ACCT_HAVE,\n" +
                "    MIN_ACCT_NUM,\n" +
                "    SMALL_CNTR_NUM,\n" +
                "    PROMOTE_END_DATE,\n" +
                "    PROMOTE_START_DATE,\n" +
                "    ACC_VAL,\n" +
                "    NAV_VAL,\n" +
                "    TRD_DATE,\n" +
                "    DRAFT_USER,\n" +
                "    PRO_TIME_LIMIT,\n" +
                "    PRO_CHA,\n" +
                "    EXT_PRO_ID,\n" +
                "    PRO_SOU,\n" +
                "    SHT_NAME,\n" +
                "    PRO_CODE,\n" +
                "    SUP_PRO_CODE,\n" +
                "    SUP_CODE,\n" +
                "    SALE_CHNLS,\n" +
                "    RISK_DISC,\n" +
                "    IS_EVA,\n" +
                "    APP_INTRO,\n" +
                "    APP_IMG,\n" +
                "    CONDUCT_IMG,\n" +
                "    DETAIL_IMG,\n" +
                "    ZONE_IMG,\n" +
                "    PROD_SPEC,\n" +
                "    ASSE_ATTR,\n" +
                "    PRO_EMP_CODE,\n" +
                "    PRO_DESC,\n" +
                "    BIG_IMG,\n" +
                "    SMALL_IMG,\n" +
                "    ENG_NAME,\n" +
                "    PRO_NAME,\n" +
                "    TRD_ID,\n" +
                "    ASSET_PRO_TYPE,\n" +
                "    LATEST_HONOR,\n" +
                "    MANAGER_INTRO,\n" +
                "    SPEC_PROM_SCHEME,\n" +
                "    SALE_PROM_ORG,\n" +
                "    DISTR_PLAN,\n" +
                "    OPEN_DAY_INST,\n" +
                "    INVE_STRATEGY,\n" +
                "    INVE_TARGET,\n" +
                "    INVE_SCOPE,\n" +
                "    INVE_MANAGER,\n" +
                "    INFO_DISC_WEB,\n" +
                "    PRO_CHAR,\n" +
                "    SUBSCRIBED_CAPI_INCRE,\n" +
                "    INCOME_DIST_DESC,\n" +
                "    PERF_RATE,\n" +
                "    TRST_RATE,\n" +
                "    MANAGE_RATE,\n" +
                "    REDEMP_RATE,\n" +
                "    BUY_RATE,\n" +
                "    OPEN_SEASON,\n" +
                "    AD_AGENCY,\n" +
                "    CUSTODIAN_USER,\n" +
                "    MANAGER_USER,\n" +
                "    REGI_ORG,\n" +
                "    EXPE_YIELD_DESC,\n" +
                "    APP_IMG_ADD,\n" +
                "    ZONE_IMG_ADD,\n" +
                "    SMALL_IMG_ADD,\n" +
                "    ASSE_ATTR_ADD,\n" +
                "    EXPE_YIELDS,\n" +
                "    CONDUCT_IMG_ADD,\n" +
                "    INVE_STYLE,\n" +
                "    DETAIL_IMG_ADD,\n" +
                "    PRO_ID,\n" +
                "    BIG_IMG_ADD,\n" +
                "    RISK_DISC_ADD,\n" +
                "    ORG_CODE,\n" +
                "    PRO_SALE_SPEC,\n" +
                "    PRO_CONTRACT,\n" +
                "    RISK_DISC_DESC,\n" +
                "    RECOMM_FLAG,\n" +
                "    IS_RESALE,\n" +
                "    MANAGER_USER_DESC,\n" +
                "    MKT_TYPE,\n" +
                "    MANAGER_VITA";
        String proStr="is_hot               \n" +
                "  ,sys_sou              \n" +
                "  ,mgr_stat             \n" +
                "  ,sale_sta             \n" +
                "  ,recomm_index         \n" +
                "  ,income_dist_way      \n" +
                "  ,charge_cls           \n" +
                "  ,currency             \n" +
                "  ,ad_flag              \n" +
                "  ,risk_lvl             \n" +
                "  ,revenue_type         \n" +
                "  ,dist_stat            \n" +
                "  ,invest_hori          \n" +
                "  ,is_grp               \n" +
                "  ,is_frozen_funds      \n" +
                "  ,res_mode             \n" +
                "  ,is_need_res          \n" +
                "  ,upd_time             \n" +
                "  ,on_shelves_date      \n" +
                "  ,pro_type             \n" +
                "  ,secondary_scale      \n" +
                "  ,priority_scale       \n" +
                "  ,inve_date            \n" +
                "  ,latest_size          \n" +
                "  ,regi_host_date       \n" +
                "  ,mini_capi_incre      \n" +
                "  ,max_part_amt         \n" +
                "  ,min_part_amt         \n" +
                "  ,par_value            \n" +
                "  ,part_price           \n" +
                "  ,ad_amt               \n" +
                "  ,face_value           \n" +
                "  ,iss_amt              \n" +
                "  ,issue_mode           \n" +
                "  ,regi_date            \n" +
                "  ,term_date            \n" +
                "  ,grp_date             \n" +
                "  ,iss_end_date         \n" +
                "  ,iss_bgn_date         \n" +
                "  ,comm_all             \n" +
                "  ,comm_1y              \n" +
                "  ,comm_year            \n" +
                "  ,inve_start           \n" +
                "  ,max_raise_size       \n" +
                "  ,min_raise_size       \n" +
                "  ,raise_issu_size      \n" +
                "  ,dura_time            \n" +
                "  ,raise_time           \n" +
                "  ,recm_end_date        \n" +
                "  ,recm_bgn_date        \n" +
                "  ,acct_have            \n" +
                "  ,min_acct_num         \n" +
                "  ,small_cntr_num       \n" +
                "  ,promote_end_date     \n" +
                "  ,promote_start_date   \n" +
                "  ,acc_val              \n" +
                "  ,nav_val              \n" +
                "  ,trd_date             \n" +
                "  ,draft_user           \n" +
                "  ,pro_time_limit       \n" +
                "  ,pro_cha              \n" +
                "  ,ext_pro_id           \n" +
                "  ,pro_sou              \n" +
                "  ,sht_name             \n" +
                "  ,pro_code             \n" +
                "  ,sup_pro_code         \n" +
                "  ,sup_code             \n" +
                "  ,sale_chnls           \n" +
                "  ,risk_disc            \n" +
                "  ,is_eva               \n" +
                "  ,app_intro            \n" +
                "  ,app_img              \n" +
                "  ,conduct_img          \n" +
                "  ,detail_img           \n" +
                "  ,zone_img             \n" +
                "  ,prod_spec            \n" +
                "  ,asse_attr            \n" +
                "  ,pro_emp_code         \n" +
                "  ,pro_desc             \n" +
                "  ,big_img              \n" +
                "  ,small_img            \n" +
                "  ,eng_name             \n" +
                "  ,pro_name             \n" +
                "  ,trd_id               \n" +
                "  ,asset_pro_type       \n" +
                "  ,latest_honor         \n" +
                "  ,manager_intro        \n" +
                "  ,spec_prom_scheme     \n" +
                "  ,sale_prom_org        \n" +
                "  ,distr_plan           \n" +
                "  ,open_day_inst        \n" +
                "  ,inve_strategy        \n" +
                "  ,inve_target          \n" +
                "  ,inve_scope           \n" +
                "  ,inve_manager         \n" +
                "  ,info_disc_web        \n" +
                "  ,pro_char             \n" +
                "  ,subscribed_capi_incre\n" +
                "  ,income_dist_desc     \n" +
                "  ,perf_rate            \n" +
                "  ,trst_rate            \n" +
                "  ,manage_rate          \n" +
                "  ,redemp_rate          \n" +
                "  ,buy_rate             \n" +
                "  ,open_season          \n" +
                "  ,ad_agency            \n" +
                "  ,custodian_user       \n" +
                "  ,manager_user         \n" +
                "  ,regi_org             \n" +
                "  ,expe_yield_desc      \n" +
                "  ,app_img_add          \n" +
                "  ,zone_img_add         \n" +
                "  ,small_img_add        \n" +
                "  ,asse_attr_add        \n" +
                "  ,expe_yields          \n" +
                "  ,conduct_img_add      \n" +
                "  ,inve_style           \n" +
                "  ,detail_img_add       \n" +
                "  ,pro_id               \n" +
                "  ,big_img_add          \n" +
                "  ,risk_disc_add        \n" +
                "  ,org_code             \n" +
                "  ,manager_user_desc    \n" +
                "  ,is_resale            \n" +
                "  ,mkt_type             \n" +
                "  ,manager_vita";
        Set<String> setSit = new HashSet<>();
        String[] split = sitStr.split(",");
        for (String s : split) {
            String trim = s.trim().toUpperCase();
            setSit.add(trim);
        }

        String[] splitPro = proStr.split(",");
        Set<String> setPro = new HashSet<>();
        for (String s : splitPro) {
            String trim = s.trim().toUpperCase();
            setPro.add(trim);
        }
//        boolean b = setSit.removeAll(setPro);
        setPro.removeAll(setSit);
        System.out.println(setPro);
    }
}
