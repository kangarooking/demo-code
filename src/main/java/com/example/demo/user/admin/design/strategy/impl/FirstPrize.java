package com.example.demo.user.admin.design.strategy.impl;

import com.example.demo.user.admin.design.strategy.LuckDrawStrategy;
import org.springframework.stereotype.Component;

@Component
public class FirstPrize implements LuckDrawStrategy {
    @Override
    public String getPrizes() {
        return "一等奖";
    }
}
