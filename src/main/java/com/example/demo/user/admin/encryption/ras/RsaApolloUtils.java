package com.example.demo.user.admin.encryption.ras;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.asymmetric.Sign;
import cn.hutool.crypto.asymmetric.SignAlgorithm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;

@Component
public class RsaApolloUtils {
    private static Logger logger = LogManager.getLogger(RsaUtils.class.getName());
    //用于加解密
    private RSA rsa;

    @Value("${rsa.publicKey}")
    private String publicKey;

    @Value("${rsa.privateKey}")
    private String privateKey;


    @PostConstruct
    public void initRsa() {
        rsa = new RSA(privateKey, publicKey);
    }

//    @ApolloConfigChangeListener("application")
//    public void rsaKeyPairChangeListener(ConfigChangeEvent changeEvent) {
//        ConfigChange changePublicKey = changeEvent.getChange("rsa.publicKey");
//        ConfigChange changePrivateKey = changeEvent.getChange("rsa.privateKey");
//        if (changePublicKey == null || changePrivateKey == null) {
//            return;
//        }
//        logger.info("rsaKeyPairChange - key: {}, oldValue: {}, newValue: {}, changeType: {}", changePublicKey.getPropertyName(), changePublicKey.getOldValue(), changePublicKey.getNewValue(), changePublicKey.getChangeType());
//        String newPrivateKey = changePrivateKey.getNewValue();
//        String newPublicKey = changePublicKey.getNewValue();
//        rsa = new RSA(newPrivateKey, newPublicKey);
//    }

    /**
     * 获取公钥
     *
     * @return
     */
    public String getPublicKey() {
        return rsa.getPublicKeyBase64();
    }

    /**
     * 私钥解密
     *
     * @param ciphertext
     * @return
     */
    public String decrypt(String ciphertext) {
        return rsa.decryptStr(ciphertext, KeyType.PrivateKey, StandardCharsets.UTF_8);
    }


    /**
     * 公钥加密
     *
     * @param targetStr
     * @return
     */
    public String encrypt(String targetStr) {
        return rsa.encryptBase64(targetStr, StandardCharsets.UTF_8, KeyType.PublicKey);
    }

    /**
     * 签名（使用自己的私钥签名）
     *
     * @param targetStr
     * @return
     */
    public String sign(String targetStr) {
        Sign sign = new Sign(SignAlgorithm.MD5withRSA, rsa.getPrivateKey(), null);
        return Base64.encodeBase64String(sign.sign(targetStr.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * 验签（使用客户端公钥验签）
     *
     * @param targetStr
     * @param ciphertext
     * @param targetPublicKey
     * @return
     */
    private boolean signVerify(String targetStr, String ciphertext, String targetPublicKey) {
        Sign sign = new Sign(SignAlgorithm.MD5withRSA, null, targetPublicKey);
        return sign.verify(targetStr.getBytes(StandardCharsets.UTF_8), Base64.decodeBase64(ciphertext.getBytes(StandardCharsets.UTF_8)));
    }
}
