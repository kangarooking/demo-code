package com.example.demo.user.admin.design.observer;

/**
 * 观察者的顶层接口
 * @param <T>
 */
public interface ObserverInterface<T> {
    //注册监听者
    public void registerListener(T t);
    //移除监听者
    public void removeListener(T t);
    //通知监听者
    public void notifyListener(DataEvent t);
}
