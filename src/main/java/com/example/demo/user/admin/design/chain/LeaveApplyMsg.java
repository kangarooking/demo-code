package com.example.demo.user.admin.design.chain;

import lombok.Data;

/**
 * 请假审批信息
 */
@Data
public class LeaveApplyMsg {
    private String msg;
    private Integer level;
    private String name;
    private Integer hour;
}
