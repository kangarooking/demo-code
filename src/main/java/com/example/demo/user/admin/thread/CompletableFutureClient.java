package com.example.demo.user.admin.thread;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * 使用可以参考：https://blog.csdn.net/jack_shuai/article/details/115304267
 */
public class CompletableFutureClient {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Double> cf = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread() + " start,time->" + System.currentTimeMillis());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
            }
            if (false) {
                throw new RuntimeException("test");
            } else {
                System.out.println(Thread.currentThread() + " exit,time->" + System.currentTimeMillis());
                return 1.2;
            }
        });
        long start = System.currentTimeMillis();
        System.out.println("main thread start,time->" +start);
        //等待子任务执行完成
        Double aDouble = cf.get();
        cf.thenAccept((result) -> {
            System.out.println("获取: " + result);
        });
        long end = System.currentTimeMillis();
        System.out.println("main thread exit,time->" + (end - start) / 1000);
        Thread.sleep(3000);

    }
}
