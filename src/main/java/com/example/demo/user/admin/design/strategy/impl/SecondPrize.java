package com.example.demo.user.admin.design.strategy.impl;

import com.example.demo.user.admin.design.strategy.LuckDrawStrategy;
import org.springframework.stereotype.Component;

@Component
public class SecondPrize implements LuckDrawStrategy {
    @Override
    public String getPrizes() {
        return "二等奖";
    }
}
