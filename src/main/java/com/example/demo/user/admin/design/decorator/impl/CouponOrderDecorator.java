package com.example.demo.user.admin.design.decorator.impl;

import com.example.demo.user.admin.design.decorator.Order;
import com.example.demo.user.admin.design.decorator.OrderDecorator;

/**
 * 优惠券装饰者
 */
public class CouponOrderDecorator implements OrderDecorator {
    @Override
    public Order decorator(Order order) {
        System.out.println("优惠券减去3元");
        order.setPay(order.getPrice() - 3);
        order.setPrice(order.getPrice() - 3);
        System.out.println("实付：" + order.getPay());
        return order;
    }
}
