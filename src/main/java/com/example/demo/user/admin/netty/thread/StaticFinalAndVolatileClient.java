package com.example.demo.user.admin.netty.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StaticFinalAndVolatileClient {
    private static int countS = 0;
    private static final int countSF = 0;
    private static volatile int countV = 0;



    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(1000);
        for (int i = 0; i < 1000; i++) {
            service.execute(()->{
                int count = countV++;
                System.out.println(count);
            });
        }

    }

}
