package com.example.demo.user.admin.encryption.ras;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * @author 一只鲨go
 * @title RSA_Test
 * @CreateTime 2021-05-13
 */


public class RsaUtils {
    private static PrivateKey privateKey;
    private static PublicKey publicKey;
    private static String algorithm = "RSA";
    private static String signAlgorithm = "MD5withRSA";

    static {
        //生成密钥对对象
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //生成密钥对
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        //生成公钥
        publicKey = keyPair.getPublic();
        System.out.println(publicKeyToString());
        //生成私钥
        privateKey = keyPair.getPrivate();
        System.out.println(privateKeyToString());
    }

    /**
     * 公钥字符串还原为公钥
     *
     * @param publicKeyString 公钥字符串
     * @return 公钥
     * @throws Exception
     */
    public static PublicKey publicKeyStringToKey(String publicKeyString) throws Exception {
        byte[] publicBytes = Base64.getDecoder().decode(publicKeyString);
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(publicBytes));
        return publicKey;
    }

    /**
     * 私钥字符串还原为私钥
     *
     * @param privateKeyString 私钥字符串
     * @return 私钥
     * @throws Exception
     */
    public static PrivateKey privateKeyStringToKey(String privateKeyString) throws Exception {
        byte[] privateBytes = Base64.getDecoder().decode(privateKeyString);
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        PrivateKey privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateBytes));
        return privateKey;
    }

    /**
     * 返回公钥字节数组
     *
     * @return
     */
    public static byte[] publicKeyEncoded() {
        return publicKey.getEncoded();
    }

    /**
     * 返回私钥字节数组
     *
     * @return
     */
    public static byte[] privateKeyEncoded() {
        return privateKey.getEncoded();
    }

    /**
     * 公钥byteToString转码
     *
     * @return
     */
    public static String publicKeyToString() {
        return Base64.getEncoder().encodeToString(publicKeyEncoded());
    }

    /**
     * 私钥byteToString转码
     *
     * @return
     */
    public static String privateKeyToString() {
        return Base64.getEncoder().encodeToString(privateKeyEncoded());
    }

    /**
     * 公钥加密
     *
     * @param input     明文
     * @param publicKey 公钥
     * @return 密文字符串
     * @throws Exception
     */
    public static String pkEncoded(String input, String publicKey) throws Exception {
        byte[] bytes = input.getBytes(StandardCharsets.UTF_8);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, publicKeyStringToKey(publicKey));
        byte[] cipherText = cipher.doFinal(bytes);
        return Base64.getEncoder().encodeToString(cipherText);
    }

    /**
     * 私钥解密
     *
     * @param cipherText 密文
     * @param privateKey 私钥
     * @return 明文字符串
     * @throws Exception
     */
    public static String skDecoded(String cipherText, String privateKey) throws Exception {
        byte[] cipherbytes = Base64.getDecoder().decode(cipherText);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, privateKeyStringToKey(privateKey));
        byte[] input = cipher.doFinal(cipherbytes);
        return new String(input);

    }

    /**
     * 数字签名：私钥加密
     *
     * @param signature  签名明文字符串
     * @param privateKey 私钥字符串
     * @return 签名字符串
     * @throws Exception
     */
    public static String skEncoded(String signature, String privateKey) throws Exception {
        Signature signature1 = Signature.getInstance(signAlgorithm);
        signature1.initSign(privateKeyStringToKey(privateKey));
        signature1.update(signature.getBytes(StandardCharsets.UTF_8));
        byte[] sign = signature1.sign();
        return Base64.getEncoder().encodeToString(sign);
    }

    /**
     * 判断签名：公钥解密
     *
     * @param input
     * @param signDate  签名密文字符串
     * @param publicKey 公钥
     * @return boolen
     * @throws Exception
     */
    public static boolean pkDecoded(String input, String signDate, String publicKey) throws Exception {

        Signature signature = Signature.getInstance(signAlgorithm);
        signature.initVerify(publicKeyStringToKey(publicKey));
        signature.update(input.getBytes(StandardCharsets.UTF_8));

        return signature.verify(Base64.getDecoder().decode(signDate));
    }

    public static void main(String[] args) throws Exception {
        String lhm = pkEncoded("123", publicKeyToString());
//        String s = skDecoded(lhm, privateKeyToString());
        System.out.println(lhm);
        System.out.println(skDecoded("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCU/7frzYdseEcIsT2/xKQpc1sN5qBjYCSFB+LnPtvmoWZhU1OZmO4vY5V/KRQHZZFaDLWfsGVrBZI4BZ/OtPG+yhrzSBKTZB9FBiLRlYSFpxrgJNuwvkWtOfKgiEm0tOybme9eJPeMVh6bPjZdjNBhQep3DnHsI+yHyDRtPGFE8QIDAQABMIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJT/t+vNh2x4RwixPb/EpClzWw3moGNgJIUH4uc+2+ahZmFTU5mY7i9jlX8pFAdlkVoMtZ+wZWsFkjgFn8608b7KGvNIEpNkH0UGItGVhIWnGuAk27C+Ra058qCISbS07JuZ714k94xWHps+Nl2M0GFB6ncOcewj7IfING08YUTxAgMBAAECgYBVXHNYKnWNVRMNDc3ckaGjBa2Ctz/n7uq9NBlwdfixloH6/CwG8TB0Ac9h/Hy3CB9PwbxD8mEIATcV1VMKr2tXBtKkxHCTTokjw5fNo85qm+JVMacN/Qr12DMBzs+5UkFT3D9It0sCTMbFRC9Q5YYj2j4oBt7eIKxkhCbPa644CQJBAPYjGxQdyljYgBjwspQjG4aNks76JA5kjtgdmv/OVKORsUshsxngYAhdq5X2K+7/2wC17DIUu+PyJ3xxmnHhpDsCQQCa+Ck17KHiwlCZ7wHI6TR+CcGL9k6SVgTtp/PcgBC67AIJ1dR0KZXTQypbNq6FpLbaW5+DZIgkFm6jUAiLDMTDAkEAr9tVNYIzWFcW7rrKXFzZBcGQ6qB9smrWREKiw7G/dz5Y1b02IX7WHiQOvA4uRYw4BMH2B7IYrDwPSjJAjq0QkQJAXVX5/DB5kEnYod4CBS/wHbliiRngWOLWNQ1jIvs/GFXtJF9VhmFFi+h4KlfU5Uk/mGR2vnxeQt3/5PzfKGCYYQJAeLvQgX2dlKJLASGi1I232ffeo9hoh965bquakuPcdzmBqpCudQyBD4WlSgbsikAJmHonUxVdaOc90rp3WUOz8Q==GK/gx1Uytikfex7w5PvTj3BJsam10Ev0wllR+Qa9kT424mSIXnzE1ATZfdQqTynM0Jk/2cpUyr/Tae5dPE2DZUMyNoLld0Pj7MxTG/T8uovhEW/cWrLDnavXsQJMu5DBL3lBCG38+zSxgckDsjB0mB/rAldbYySA/vYXHHt+JhU=", privateKeyToString()));
//        System.out.println(s);
//        String lhm1 = skEncoded("lhm", privateKeyToString());
//        boolean lhm2 = pkDecoded("lhm", lhm1, publicKeyToString());
//        System.out.println(lhm1);
//        System.out.println(lhm2);
    }

}

