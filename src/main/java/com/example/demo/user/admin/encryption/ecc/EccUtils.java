package com.example.demo.user.admin.encryption.ecc;


import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.util.Base64;

public class EccUtils {

    private static final String ECIES_ALGORITHM = "ECIES";
    private static final String EC_PROVIDER = "BC";

    private static final String SIGNATURE = "SHA256withECDSA";

    private static PrivateKey privateKey;

    private static PublicKey publicKey;

    static {
        try {
            Security.addProvider(new BouncyCastleProvider());
            // 获取指定算法的密钥对生成器
            KeyPairGenerator generator = KeyPairGenerator.getInstance(ECIES_ALGORITHM, EC_PROVIDER);
            // 初始化密钥对生成器（指定密钥长度, 使用默认的安全随机数源）
            generator.initialize(256);
            // 随机生成一对密钥（包含公钥和私钥）
            KeyPair keyPair = generator.generateKeyPair();
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 返回公钥字节数组
     *
     * @return
     */
    public static byte[] publicKeyEncoded() {
        return publicKey.getEncoded();
    }

    /**
     * 返回私钥字节数组
     *
     * @return
     */
    public static byte[] privateKeyEncoded() {
        return privateKey.getEncoded();
    }

    /**
     * 公钥byteToString转码
     *
     * @return
     */
    public static String publicKeyToString() {
        return Base64.getEncoder().encodeToString(publicKeyEncoded());
    }

    /**
     * 私钥byteToString转码
     *
     * @return
     */
    public static String privateKeyToString() {
        return Base64.getEncoder().encodeToString(privateKeyEncoded());
    }

    /**
     * ECC 加密
     *
     * @param publicKey 公钥
     * @param content   原文
     * @return　密文
     */
    public static String eccEncrypt(PublicKey publicKey, String content) {
        try {
            byte[] bytes = content.getBytes(StandardCharsets.UTF_8);
            Cipher cipher = Cipher.getInstance(ECIES_ALGORITHM, EC_PROVIDER);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] cipherText = cipher.doFinal(bytes);
            return Base64.getEncoder().encodeToString(cipher.doFinal(cipherText));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * ECC 解密
     *
     * @param privateKey 　私钥
     * @param encrypted  　密文
     * @return　原文
     */
    public static String eccDecrypt(PrivateKey privateKey, String encrypted) {
        try {
            byte[] cipherbytes = Base64.getDecoder().decode(encrypted);
            Cipher cipher = Cipher.getInstance(ECIES_ALGORITHM, EC_PROVIDER);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] input = cipher.doFinal(cipherbytes);
            return new String(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args) {
        // 测试文本
        String content = "123";

        // 生成密钥对
        String s = eccEncrypt(publicKey, content);
        System.out.println(eccDecrypt(privateKey, s));

    }

//        // 测试文本
//        byte[] plain1 = "123".getBytes();
//
//        // 生成密钥对
//        KeyPair keyPair1 = generateECCKeyPair(256);
//        PublicKey publicKey1 = keyPair.getPublic();
//        PrivateKey privateKey1 = keyPair.getPrivate();
//
//        // 签名验签
//        byte[] sign1 = eccSign(privateKey1, plain1);
//        boolean verify = eccVerify(publicKey1, plain1, sign1);
//        System.err.println(verify);


    /**
     * 私钥签名
     *
     * @param privateKey 私钥
     * @param plain      原文
     * @return 签名
     */
    public static byte[] eccSign(PrivateKey privateKey, byte[] plain) {
        try {
            Signature signature = Signature.getInstance(SIGNATURE);
            signature.initSign(privateKey);
            signature.update(plain);
            return signature.sign();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 公钥验签
     *
     * @param publicKey 公钥
     * @param plain     原文
     * @param sign      签名
     * @return
     */
    public static boolean eccVerify(PublicKey publicKey, byte[] plain, byte[] sign) {
        try {
            Signature signature = Signature.getInstance(SIGNATURE);
            signature.initVerify(publicKey);
            signature.update(plain);
            return signature.verify(sign);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
