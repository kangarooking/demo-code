package com.example.demo.user.admin.design.chain.impl;

import com.example.demo.user.admin.design.chain.ApplyChain;

/**
 * 默认审批流程
 */
public class DefaultApplyChain extends ApplyChain {
    @Override
    protected void exc(Object obj) {
        //使用链头开始调用整条责任链
        System.out.println("--------------- start --------------");
        first.transform(obj);
    }
}
