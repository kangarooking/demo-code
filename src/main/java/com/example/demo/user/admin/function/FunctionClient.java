package com.example.demo.user.admin.function;

public class FunctionClient {
    public static String get(MyFunction<String> function, String name, Integer age) {
        //省略XXXX逻辑
        return function.test1(name, age);
    }

    public static void main(String[] args) {
        //这里的(name, age)相当于重写test1方法的形参
        get((name, age) -> {
            System.out.println("我是函数式写法，我相当于MyFunction接口的一个实现类");
            System.out.println("这里是MyFunction.test1的重写方法");
            return "返回值";
        }, "kangarooking", 24);

        /********************************************/

        get(new MyFunction<String>() {
            @Override
            public String test1(String name, Integer age) {
                System.out.println("我是匿名内部类写法，我也相当于MyFunction接口的一个实现类");
                System.out.println("这里是MyFunction.test1的重写方法");
                return "返回值";
            }
        }, "kangarooking", 20);

        /************* 或者还有小伙伴没有明白的，看下面 ***************/

        MyFunction<String> myFunction = new MyFunction<String>() {
            @Override
            public String test1(String name, Integer age) {
                System.out.println("我是匿名内部类写法，我也相当于MyFunction接口的一个实现类");
                System.out.println("这里是MyFunction.test1的重写方法");
                return "返回值";
            }
        };
        get(myFunction, "kangarooking", 20);
    }
}
