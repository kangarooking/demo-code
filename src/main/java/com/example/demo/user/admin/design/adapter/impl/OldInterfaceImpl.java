package com.example.demo.user.admin.design.adapter.impl;

import com.example.demo.user.admin.design.adapter.OldInterface;

/**
 * 老接口实现类
 */
//@Component
public class OldInterfaceImpl implements OldInterface {
    @Override
    public void add(Integer id) {
        System.out.println("调用老接口");
    }
}
