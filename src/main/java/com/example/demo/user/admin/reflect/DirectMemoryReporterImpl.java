package com.example.demo.user.admin.reflect;


import cn.hutool.core.thread.NamedThreadFactory;
import com.example.demo.user.admin.thread.CustomThreadFactory;
import io.netty.util.internal.PlatformDependent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 通过反射获取netty框架中，PlatformDependent类的成员变量值，来监测netty堆外内存的使用情况
 */
@Slf4j
@Component
public class DirectMemoryReporterImpl {
    private static final int _1K = 1024;
    private static final String BUSINESS_KEY = "netty_direct_memory";

    private AtomicLong directMemory;
    private long directMemoryLimit;

    @PostConstruct
    public void init() {
        Field direct_memory_counter = ReflectionUtils.findField(PlatformDependent.class, "DIRECT_MEMORY_COUNTER");
        Field limit = ReflectionUtils.findField(PlatformDependent.class, "DIRECT_MEMORY_LIMIT");
        direct_memory_counter.setAccessible(true);
        limit.setAccessible(true);
        try {
            directMemory = (AtomicLong) direct_memory_counter.get(PlatformDependent.class);
            directMemoryLimit = (long) limit.get(PlatformDependent.class);
            log.info(">>>>>>>>>>>directMemoryLimit={}", directMemoryLimit);
            startReport();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void startReport() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1, new CustomThreadFactory());
        scheduledExecutorService.scheduleAtFixedRate(this::doReport, 0, 10, TimeUnit.SECONDS);
    }

    private void doReport() {
        try {
            int memoryInKb = (int) (directMemory.get() / _1K);
            log.info("{}: {}k", BUSINESS_KEY, memoryInKb);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
