package com.example.demo.user.admin.encryption.ras;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.asymmetric.Sign;
import cn.hutool.crypto.asymmetric.SignAlgorithm;
import cn.hutool.http.ContentType;
import cn.hutool.http.HttpUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import sun.misc.BASE64Decoder;

import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class RSAHutoolUtils {
//    private static Logger logger = LogManager.getLogger(RsaUtils.class.getName());
//    //用于加解密
//    private static final RSA rsa = SecureUtil.rsa();
//    //用于签名和验签
//    private static final Sign sign = SecureUtil.sign(SignAlgorithm.MD5withRSA);
//
//
//    // 己方私钥
//    private static String PRIVATE_KEY = "MIIJQwIBADANBgkqhkiG";
//    // 合作方公钥
//    private static String PUBLIC_KEY  = "MIIBIjANBgkqhkiG9w0B";
//
//    private static String local_url    = "http://localhost:8080/service/123";
//
//
//    public static void main(String[] args) {
//        // 业务参数
//        Map<String, Object> bizMap = new HashMap<>();
//        bizMap.put("mobile", "15311281112");
//        bizMap.put("certNo", "130625199210101010");
//        bizMap.put("name", "张三");
//        bizMap.put("channelNo", "2865010");
//        // 网关请求
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("appKey", "xxxxxxxxxx");
//        map.put("serviceName", "xxxxxxxxx");
//        map.put("bizContent", encryptedByPubKey(bizMap));
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//
//        map.put("format", "json");
//        map.put("signType", "RSA");
//        map.put("charset", "UTF-8");
//        map.put("version", "1.0.0");
//        map.put("sign", signByPriKey(map));
//        // 发送请求
//        System.out.println("请求入参：" + map.toString());
//        String resp = HttpUtil.createPost(local_url)
//                .body(map.toString(), ContentType.JSON.toString())
//                .execute()
//                .body();
//        System.out.println("请求出参：" + resp);
//        // 验签
//        if (!checkSignByPubKey(GsonUtil.fromJson2Map(resp))) {
//            System.out.println("签名错误");
//            return;
//        }
//        // 解密
//        String bizContents = decryptByPriKey(resp);
//        System.out.println("解密：" + bizContents);
//    }
//
//    /**
//     * 合作方返回结果时，会用我方公钥加密
//     * 我们收到返回结果时，用我方私钥解密
//     */
//    private static String decryptByPriKey(String resp) {
//        RSA rsa = new RSA(PRIVATE_KEY, null);
//        Map<String, Object> map1 = GsonUtil.fromJson2Map(resp);
//        return rsa.decryptStr(map1.get("bizContent").toString(), KeyType.PrivateKey);
//    }
//
//    /**
//     * 合作方返回结果时，会用合作方私钥加签
//     * 我们收到返回结果时，用合作方公钥验签
//     */
//    private static boolean checkSignByPubKey(Map<String, Object> respMap) {
//        String signValue = (String) respMap.get("sign");
//        respMap.remove("sign");
//        String s = GsonUtil.toJsonString(respMap);
//        Sign sign = new Sign(SignAlgorithm.SHA1withRSA, null, PUBLIC_KEY);
//        return sign.verify(s.getBytes(StandardCharsets.UTF_8), Base64.decodeBase64(signValue.getBytes(StandardCharsets.UTF_8)));
//    }
//
//    /**
//     * 合作方公钥加密
//     * 合作方收到请求后，用合作方私钥解密
//     */
//    private static Object encryptedByPubKey(Map<String, Object> bizMap) {
//        TreeMap<String, Object> sort = MapUtil.sort(bizMap);
//        String sortJson = GsonUtil.toJsonString(sort);
//        RSA pubRsa = new RSA(null, PUBLIC_KEY);
//        byte[] encryptedData = pubRsa.encrypt(sortJson.getBytes(StandardCharsets.UTF_8), KeyType.PublicKey);
//        return Base64.encodeBase64String(encryptedData);
//    }
//
//    /**
//     * 己方私钥加签
//     * 合作方收到请求后，用我方公钥验签
//     */
//    public static String signByPriKey(Map<String, Object> params) {
//        TreeMap<String, Object> sort = MapUtil.sort(params);
//        String paramsJson = GsonUtil.toJsonString(sort);
//        Sign sign = new Sign(SignAlgorithm.SHA1withRSA, PRIVATE_KEY, null);
//        return Base64.encodeBase64String(sign.sign(paramsJson.getBytes(StandardCharsets.UTF_8)));
//    }
//
//    /**
//     * 获取公钥
//     *
//     * @return
//     */
//    public static String getPublicKey() {
//        return rsa.getPublicKeyBase64();
//    }
//
//    /**
//     * 私钥解密
//     *
//     * @param ciphertext
//     * @return
//     */
//    public static String decrypt(String ciphertext) {
//        return rsa.decryptStr(ciphertext, KeyType.PrivateKey, StandardCharsets.UTF_8);
//    }
//
//
//    /**
//     * 公钥加密
//     *
//     * @param targetStr
//     * @return
//     */
//    public static String encrypt(String targetStr) {
//        return rsa.encryptBase64(targetStr, StandardCharsets.UTF_8, KeyType.PublicKey);
//    }
//
//    /**
//     * 签名（私钥加密）
//     *
//     * @param targetStr
//     * @return
//     */
//    public static byte[] sign(String targetStr) {
//        return sign.sign(StrUtil.bytes(targetStr, StandardCharsets.UTF_8));
//    }
//
//
//
//    /**
//     * 验签（公钥解密，验证数据是否被篡改）
//     *
//     * @param targetStr
//     * @return
//     */
//    public static Boolean signVerify(String targetStr, byte[] ciphertext, String targetPublicKey) {
//        //生成密钥对对象
//        KeyPairGenerator keyPairGenerator = null;
//        try {
//            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        //生成密钥对
//        KeyPair keyPair = keyPairGenerator.generateKeyPair();
//        //生成公钥
//        PublicKey publicKey = keyPair.getPublic();
////        Sign sign = SecureUtil.sign(SignAlgorithm.MD5withRSA);
//        try {
//            rsa.setPublicKey(publicKey);
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        }
//        return sign.verify(StrUtil.bytes(targetStr), ciphertext);
//    }
//
//    /**
//     * 公钥字符串还原为公钥
//     *
//     * @param publicKeyString 公钥字符串
//     * @return 公钥
//     * @throws Exception
//     */
//    public static PublicKey publicKeyStringToKey(String publicKeyString) throws Exception {
//        byte[] publicBytes = cn.hutool.core.codec.Base64.decode(publicKeyString);
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        return keyFactory.generatePublic(new X509EncodedKeySpec(publicBytes));
//    }
//    public static PublicKey getRSAPublidKeyBybase64(String base64s)throws Exception {
//        X509EncodedKeySpec keySpec =new X509EncodedKeySpec((new BASE64Decoder()).decodeBuffer(base64s));
//        PublicKey publicKey = null;
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        try{
//            publicKey =keyFactory.generatePublic(keySpec);
//        }catch(InvalidKeySpecException var4){
//        }
//        return publicKey;
//    }
}
