package com.example.demo.user.admin.disruptor;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.util.DaemonThreadFactory;

import java.nio.ByteBuffer;

public class OrderEventMain {
    public static void main(String[] args) throws Exception {
        int bufferSize = 16;

        Disruptor<OrderEvent> disruptor =
                new Disruptor<>(OrderEvent::new, bufferSize, DaemonThreadFactory.INSTANCE);

//        disruptor.handleEventsWith(new OrderEventHandler());
        disruptor.handleEventsWithWorkerPool((orderEvent -> {
            System.out.println("不可重复消费1 消息="+orderEvent.getOrderId());
        }));

        disruptor.start();


        RingBuffer<OrderEvent> ringBuffer = disruptor.getRingBuffer();
        ByteBuffer bb = ByteBuffer.allocate(8);
        for (long l = 0; true; l++) {
            bb.putLong(0, l);
            ringBuffer.publishEvent((event, sequence, buffer) -> event.setOrderId(buffer.getLong(0)), bb);
            Thread.sleep(1000);
        }
    }
}
