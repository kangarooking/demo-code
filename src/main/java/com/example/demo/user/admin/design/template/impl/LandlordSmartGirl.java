package com.example.demo.user.admin.design.template.impl;

import com.example.demo.user.admin.design.template.RichFather;

/**
 * 女儿
 */
public class LandlordSmartGirl extends RichFather {
    @Override
    protected void marry() {
        System.out.println("找了一个穷小子结婚");
    }

    @Override
    protected void cause() {
        System.out.println("现在接手了富爹的公司，成为了女总裁");
    }
}
