package com.example.demo.user.admin.design.decorator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private Double price;
    private Integer orderId;
    private String orderNo;
    private Double pay;
    private List<Integer> promotionTypeList;
}
