package com.example.demo.user.admin.controller;

import com.example.demo.user.admin.encryption.ras.RsaUtils;
import com.example.demo.user.admin.vo.LoginAO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String getPKey() throws Exception {
        String lhm = RsaUtils.skEncoded("lhm", RsaUtils.privateKeyToString());
        return RsaUtils.publicKeyToString() + ",,," + lhm;
    }

    @PostMapping
    public Boolean vif(@RequestBody LoginAO loginAO) throws Exception {
        String s1 = loginAO.getSign().replaceAll(" ", "+");
        String s2 = loginAO.getPassword().replaceAll(" ", "+");
        boolean lhm = RsaUtils.pkDecoded("lhm", s1, RsaUtils.publicKeyToString());
        String s = RsaUtils.skDecoded(s2, RsaUtils.privateKeyToString());
        return lhm && "123".equals(s);
    }

}
