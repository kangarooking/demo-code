package com.example.demo.user.admin.design.adapter;

import org.springframework.stereotype.Component;

/**
 * 新接口
 */
@Component
public class NewInterface {
    //新接口方法
    public void save(Integer id, String name) {
        System.out.println("调用新接口");
    }
}
