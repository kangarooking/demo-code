package com.example.demo.user.admin.design.decorator;

/**
 * 活动枚举
 */
public enum PromotionType {
    COUPON(1),
    VIP(2);
    private int type;

    PromotionType(final int type){
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
