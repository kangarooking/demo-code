package com.example.demo.user.admin.function;

/**
 * 这里的@FunctionalInterface可加可不加
 * 它只是检查与标识当前接口是否是一个函数式接口
 * @param <T>
 */
@FunctionalInterface
public interface MyFunction<T> {
    T test1(String name, Integer age);
}
