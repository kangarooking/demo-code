package com.example.demo.user.admin.design.strategy.impl;

import com.example.demo.user.admin.design.strategy.LuckDrawStrategy;
import org.springframework.stereotype.Component;

@Component
public class ThirdPrize implements LuckDrawStrategy {
    @Override
    public String getPrizes() {
        return "三等奖";
    }
}
