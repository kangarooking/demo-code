package com.example.demo.user.admin.reflect;

import java.util.HashSet;
import java.util.Set;

public class TargetA {
    private Set<Integer> keySet = new HashSet<>();

    public Set<Integer> getKeySet() {
        return keySet;
    }

    public void setKeySet(Set<Integer> keySet) {
        this.keySet = keySet;
    }
}
