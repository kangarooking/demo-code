package com.example.demo.user.admin.design.strategy;

/**
 * 抽奖策略接口
 */
public interface LuckDrawStrategy {
    //获取奖品的抽象方法
    String getPrizes();
}
