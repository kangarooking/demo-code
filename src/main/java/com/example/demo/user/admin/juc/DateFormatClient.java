package com.example.demo.user.admin.juc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * SimpleDateFormat thread unsafe
 */
public class DateFormatClient {

    public static void main(String[] args) {
        ThreadLocal<String> stringThreadLocal = new ThreadLocal<>();
        stringThreadLocal.set("kangarooKing");
        String s = stringThreadLocal.get();
        ThreadLocal<SimpleDateFormat> threadLocalDateFormat = new ThreadLocal<SimpleDateFormat>() {
            @Override
            protected SimpleDateFormat initialValue() {
                return new SimpleDateFormat("yyyyMMddHHmm");
            }
        };
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
        ExecutorService service = Executors.newFixedThreadPool(500);
        while (true) {
            service.execute(() -> {
                try {
                    Date date = new Date(Math.abs(new Random().nextLong()));
                    String format1 = format.format(date);
                    String format2 = format.format(date);
                    if (!format1.equals(format2)){
                        System.out.println("SimpleDateFormat.format thread unsafe");
                    }
                    SimpleDateFormat simpleDateFormat = threadLocalDateFormat.get();
                    simpleDateFormat.parse("20210925095933");
                    String format3 = simpleDateFormat.format(date);
                    String format4 = simpleDateFormat.format(date);
                    if (!format3.equals(format4)){
                        System.out.println("SimpleDateFormat.format use ThreadLocal unsafe");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
