package com.example.demo.user.admin.vo;

import lombok.Data;

import java.util.Date;

@Data
public class UserDO {
    private String name;
    private String code;
    private String cType;
    private Integer addN;
    private String content;
    private Date createDate;
    private Integer status;
    private String userId;
}
