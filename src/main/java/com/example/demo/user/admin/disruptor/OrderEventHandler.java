package com.example.demo.user.admin.disruptor;

import com.lmax.disruptor.EventHandler;

public class OrderEventHandler implements EventHandler<OrderEvent> {
    @Override
    public void onEvent(OrderEvent orderEvent, long sequence, boolean endOfBatch) throws Exception {
        System.out.println("orderEvent=" + orderEvent + "  sequence=" + sequence + "  endOfBatch=" + endOfBatch);
    }
}
