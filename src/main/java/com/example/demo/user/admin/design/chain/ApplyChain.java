package com.example.demo.user.admin.design.chain;

import lombok.Data;

/**
 * 责任链的管理者
 * 管理链头尾，以及链条的增删改查
 */
@Data
public abstract class ApplyChain {
    public ApplyHandler<?> first = new ApplyHandler<Object>() {
        @Override
        public void handle(Object obj) {
            //第一个节点不参与处理，直接往下申请
            apply(obj);
        }
    };
    private ApplyHandler<?> end = first;

    //责任链的调用入口
    protected abstract void exc(Object obj);

    //向尾部添加节点
    public void addLast(ApplyHandler<?> handler){
        end.setNext(handler);
        end = handler;
    }

    //...此处省略了其他操作责任链的方法
}
