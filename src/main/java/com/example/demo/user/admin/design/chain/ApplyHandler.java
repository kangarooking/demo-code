package com.example.demo.user.admin.design.chain;

import lombok.Data;

/**
 * 责任链中的节点（流程中的审批人）
 * @param <T>
 * 这里将审批的信息定义为泛型，更灵活
 */
@Data
public abstract class ApplyHandler<T> {
    protected ApplyHandler<?> next = null;
    //定义该审批人在整个责任链中的审批顺序
    protected Integer order;
    protected String name;

    public void apply(Object obj) {
        if (null != next) {
            next.transform(obj);
        }else {
            System.out.println("-----------------------------");
            System.out.println("end");
        }
    }

    @SuppressWarnings("unchecked")
    public void transform(Object obj){
        //将传入的对象转换为泛型定义的类型
        T param = (T)obj;
        handle(param);
    }

    //具体的审批逻辑
    protected abstract void handle(T param);

    public ApplyHandler(Integer order) {
        this.order = order;
    }

    public ApplyHandler() {
    }
}
