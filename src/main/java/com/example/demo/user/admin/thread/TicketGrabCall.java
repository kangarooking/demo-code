package com.example.demo.user.admin.thread;

import java.util.concurrent.Callable;

public class TicketGrabCall implements Callable<String> {
    @Override
    public String call() throws Exception {
        Thread.sleep(2000);
        return "end";
    }
}
