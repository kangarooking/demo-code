package com.example.demo.user.admin.juc;



import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MapClient {
    public static void main(String[] args) throws InterruptedException {
        Map<String, String> map = new ConcurrentHashMap<>();
//        Map<Integer, String> map = new HashMap<>();

        ExecutorService executorService = new ThreadPoolExecutor(5, 5,
                60, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(1000), new ThreadPoolExecutor.CallerRunsPolicy());
        for (int i = 0; i < 100; i++) {
            int finalI = i;
            executorService.execute(() -> {
                for (int j = 0; j < 10; j++) {
                    String key = finalI * 10 + j + "";
                    String value = finalI + j + "";
                    map.put(key, value);
                    System.out.println(Thread.currentThread().getName() + " put key=" + key + " value=" + value);
                }
            });
        }
        Thread.sleep(3000);
        System.out.println(map);
        System.out.println(map.size());
    }


//    private final void addCount(long x, int check) {
//        ConcurrentHashMap.CounterCell[] as; long b, s;
//        //判断counterCells是否为空，
//        //1).不管是否为空，都会先通过cas操作尝试修改baseCount变量，对这个变量进行原子累加操作(注：如果在没有竞争的情况下，仍然采用baseCount来记录元素个数)
//        //2).如果本次cas记录失败说明存在竞争，就不使用baseCount来累加，而是使用CounterCell[]这个对象数组来记录
//        //理解：其实就是先cas给baseCount累加，如果累加失败，就表示有竞争，然后转而使用CounterCell[]这种分段加锁的方式
//        //来提高多线程竞争激烈情况下的处理效率，juc下另一个工具LongAdder的实现方式与之类似。
//        if ((as = counterCells) != null ||
//                !U.compareAndSwapLong(this, BASECOUNT, b = baseCount, s = b + x)) {
//            ConcurrentHashMap.CounterCell a; long v; int m;
//            boolean uncontended = true;
//            //这里有几个判断
//            //1. 计数表为空则直接调用fullAddCount
//            //2. 从计数表中随机取出一个数组的位置为空，直接调用fullAddCount
//            //3. 通过CAS修改CounterCell随机位置的值，如果修改失败说明出现并发情况（这里又用到了一种巧妙的方法），调用fullAndCount
//            //Random在线程并发的时候会有性能问题以及可能会产生相同的随机数,ThreadLocalRandom.getProbe可以解决这个问题，并且性能要比Random高
//            if (as == null || (m = as.length - 1) < 0 ||
//                    (a = as[ThreadLocalRandom.getProbe() & m]) == null ||
//                    !(uncontended = U.compareAndSwapLong(a, CELLVALUE, v = a.value, v + x))) {
//                fullAddCount(x, uncontended);
//                return;
//            }
//            /**
//             * if (check <= 1)
//             * 执行这个代码的前提是程序进入这个判断if ((as = counterCells) != null || !U.compareAndSwapLong(this, BASECOUNT, b = baseCount, s = b + x))
//             * 进入这个判断代表当前counterCells!=null 或者 cas原子累加失败--线程竞争激烈，竞争这么激烈的情况下，如果当前节点的链表长度还小于等于1，那么证明当前剩余空间还很多
//             * 本次就直接返回，都不需要去检查是否扩容
//             */
//            if (check <= 1)
//                //如果链表长度小于等于1，直接就不考虑扩容
//                return;
//            s = sumCount();
//        }
//        //如果链表长度大于等于0，检查是否需要扩容，或帮助扩容
//        if (check >= 0) {
//            ConcurrentHashMap.Node<K,V>[] tab, nt; int n, sc;
//            //s标识集合大小，如果集合大小大于或等于扩容阈值（默认值的0.75）
//            //并且table不为空并且table的长度小于最大容量
//            while (s >= (long)(sc = sizeCtl) && (tab = table) != null &&
//                    (n = tab.length) < MAXIMUM_CAPACITY) {
//                int rs = resizeStamp(n);
//                if (sc < 0) {
//                    //这5个条件只要有一个条件为true，说明当前线程不能帮助进行此次的扩容，直接跳出循环
//                    if ((sc >>> RESIZE_STAMP_SHIFT) != rs || sc == rs + 1 ||
//                            sc == rs + MAX_RESIZERS || (nt = nextTable) == null ||
//                            transferIndex <= 0)
//                        break;
//                    //这里是帮助扩容
//                    if (U.compareAndSwapInt(this, SIZECTL, sc, sc + 1))
//                        transfer(tab, nt);
//                }
//                // 本次第一个线程去扩容
//                // 如果当前没有在扩容，那么rs肯定是一个正数，通过rs<<RESIZE_STAMP_SHIFT 将sc设置为一个负数，+2 表示有一个线程在执行扩容
//                else if (U.compareAndSwapInt(this, SIZECTL, sc,
//                        (rs << RESIZE_STAMP_SHIFT) + 2))
//                    transfer(tab, null);
//                s = sumCount();
//            }
//        }
//    }


//    private final void transfer(Node<K,V>[] tab, Node<K,V>[] nextTab) {
//        int n = tab.length, stride;
//        //stride 步长（也就是扩容时，每个线程分配的桶数，最小为16）
//        //根据cpu核数计算步长
//        if ((stride = (NCPU > 1) ? (n >>> 3) / NCPU : n) < MIN_TRANSFER_STRIDE)
//            stride = MIN_TRANSFER_STRIDE; // subdivide range
//        if (nextTab == null) {            // initiating
//            try {
//                //初始化一个原数组两倍大小的新数组
//                @SuppressWarnings("unchecked")
//                Node<K,V>[] nt = (Node<K,V>[])new Node<?,?>[n << 1];
//                nextTab = nt;
//            } catch (Throwable ex) {      // try to cope with OOME
//                sizeCtl = Integer.MAX_VALUE;
//                return;
//            }
//            nextTable = nextTab;
//            transferIndex = n;
//        }
//        int nextn = nextTab.length;
//        //初始化 转移节点 其hash值为MOVED=-1
//        ForwardingNode<K,V> fwd = new ForwardingNode<K,V>(nextTab);
//        boolean advance = true;
//        boolean finishing = false; // to ensure sweep before committing nextTab
//        //i为老数组当前转移位置的下标，bound为边界，标识当前线程转移到什么位置停止本次的转移（又要重新cas获取转移区间）
//        //也就是会转移老数组下标[bound,i]区间内的节点，每cas一次transferIndex就会-16
//        // （这样就保证了后面来的线程不会和前面的线程获取到同一个区间）
//        //例如：当前i=32（表示当前线程正在转移老数组下标为32的节点），每转移一次i-1（从后往前的一个转移顺序）
//        //bound=16，代表当前线程从下标为32的节点开始往前转移，直到下标为16，当前线程本次转移结束。
//        for (int i = 0, bound = 0;;) {
//            Node<K,V> f; int fh;
//            while (advance) {
//                int nextIndex, nextBound;
//                if (--i >= bound || finishing)
//                    advance = false;
//                else if ((nextIndex = transferIndex) <= 0) {
//                    i = -1;
//                    advance = false;
//                }
//                //当本次[bound,i]区间内的节点转移完毕，又重cas获取新的区间，
//                else if (U.compareAndSwapInt
//                        (this, TRANSFERINDEX, nextIndex,
//                                nextBound = (nextIndex > stride ?
//                                        nextIndex - stride : 0))) {
//                    bound = nextBound;
//                    i = nextIndex - 1;
//                    advance = false;
//                }
//            }
//            if (i < 0 || i >= n || i + n >= nextn) {
//                int sc;
//                /**
//                 这里是最后一个线程退出transfer方法的地方
//                 在退出前会，最后一个线程会从老数组的[0,length]区间再执行一遍节点转移
//                 猜测是为了保证所有的节点都转移成功了，如果在区间转移的过程中遇到转移节点
//                 那么会跳过。
//                 */
//                if (finishing) {
//                    nextTable = null;
//                    table = nextTab;
//                    sizeCtl = (n << 1) - (n >>> 1);
//                    return;
//                }
//                /**
//                 第一个扩容的线程，进入transfer方法前，会执行 sizeCtl = (resizeStamp(n) << RESIZE_STAMP_SHIFT) + 2)
//                 后面帮助扩容的线程，进入transfer方法前，会执行 sizeCtl = sizeCtl+1
//                 每一个退出transfer方法的线程，退出之前，会执行 sizeCtl = sizeCtl-1也就是下面的cas代码 U.compareAndSwapInt(this, SIZECTL, sc = sizeCtl, sc - 1)
//                 所以直到最后一个线程退出时：
//                 必定有 sc == (resizeStamp(n) << RESIZE_STAMP_SHIFT) + 2)，也就是下面的判断 (sc - 2) == resizeStamp(n) << RESIZE_STAMP_SHIFT
//                 如果不相等，说明不到最后一个线程，直接return出transfer方法
//                 */
//                if (U.compareAndSwapInt(this, SIZECTL, sc = sizeCtl, sc - 1)) {
//                    if ((sc - 2) != resizeStamp(n) << RESIZE_STAMP_SHIFT)
//                        return;
//                    finishing = advance = true;
//                    //最后一个线程就是从这里将原数组长度n赋值给i，然后又重新遍历检查一遍。
//                    i = n; // recheck before commit
//                }
//            }
//            else if ((f = tabAt(tab, i)) == null)
//                advance = casTabAt(tab, i, null, fwd);
//            else if ((fh = f.hash) == MOVED)
//                //判断如果是转移节点就跳过
//                advance = true; // already processed
//            else {
//                //开始迁移节点（f是桶位上的头结点）
//                synchronized (f) {
//                    if (tabAt(tab, i) == f) {
//                        Node<K,V> ln, hn;
//                        if (fh >= 0) {
//                            /** 将原来的一个链表拆成两个
//                             * 一个放在原数组下标位置
//                             * 另一个放在原数组下标+原数组长度的位置
//                             */
//                            int runBit = fh & n;
//                            Node<K,V> lastRun = f;
//                            //循环查找，找到链表中最后一个和它的上一个结点hash & n 所得值不同的节点
//                            //作为lastRun，其hash & n值为runBit
//                            for (Node<K,V> p = f.next; p != null; p = p.next) {
//                                int b = p.hash & n;
//                                if (b != runBit) {
//                                    runBit = b;
//                                    lastRun = p;
//                                }
//                            }
//                            //为下面的循环计算赋初值
//                            if (runBit == 0) {
//                                ln = lastRun;
//                                hn = null;
//                            }
//                            else {
//                                hn = lastRun;
//                                ln = null;
//                            }
//                            //循环组装两个链表（看情况，有可能最后有两个链表，
//                            // 也可能只有一个链表，也可能就只有一个头结点）
//                            for (Node<K,V> p = f; p != lastRun; p = p.next) {
//                                int ph = p.hash; K pk = p.key; V pv = p.val;
//                                if ((ph & n) == 0)
//                                    ln = new Node<K,V>(ph, pk, pv, ln);
//                                else
//                                    hn = new Node<K,V>(ph, pk, pv, hn);
//                            }
//                            //将一个链表放到新数组（位置：原数组下标为）
//                            setTabAt(nextTab, i, ln);
//                            //将另一个链表放到新数组（位置：原数组下标+原数组长度）
//                            setTabAt(nextTab, i + n, hn);
//                            setTabAt(tab, i, fwd);
//                            advance = true;
//                        }
//                        //迁移红黑树
//                        else if (f instanceof ConcurrentHashMap.TreeBin) {
//                            ConcurrentHashMap.TreeBin<K,V> t = (ConcurrentHashMap.TreeBin<K,V>)f;
//                            TreeNode<K,V> lo = null, loTail = null;
//                            TreeNode<K,V> hi = null, hiTail = null;
//                            int lc = 0, hc = 0;
//                            for (Node<K,V> e = t.first; e != null; e = e.next) {
//                                int h = e.hash;
//                                TreeNode<K,V> p = new TreeNode<K,V>
//                                        (h, e.key, e.val, null, null);
//                                if ((h & n) == 0) {
//                                    if ((p.prev = loTail) == null)
//                                        lo = p;
//                                    else
//                                        loTail.next = p;
//                                    loTail = p;
//                                    ++lc;
//                                }
//                                else {
//                                    if ((p.prev = hiTail) == null)
//                                        hi = p;
//                                    else
//                                        hiTail.next = p;
//                                    hiTail = p;
//                                    ++hc;
//                                }
//                            }
//                            ln = (lc <= UNTREEIFY_THRESHOLD) ? untreeify(lo) :
//                                    (hc != 0) ? new ConcurrentHashMap.TreeBin<K,V>(lo) : t;
//                            hn = (hc <= UNTREEIFY_THRESHOLD) ? untreeify(hi) :
//                                    (lc != 0) ? new ConcurrentHashMap.TreeBin<K,V>(hi) : t;
//                            setTabAt(nextTab, i, ln);
//                            setTabAt(nextTab, i + n, hn);
//                            setTabAt(tab, i, fwd);
//                            advance = true;
//                        }
//                    }
//                }
//            }
//        }
//    }
}
