package com.example.demo.user.admin.design.decorator;

/**
 * 订单装饰者接口
 */
public interface OrderDecorator {
    //抽象装饰方法
    Order decorator(Order order);
}
