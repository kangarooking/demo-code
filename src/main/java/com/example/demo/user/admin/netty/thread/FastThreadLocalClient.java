package com.example.demo.user.admin.netty.thread;

import io.netty.util.concurrent.FastThreadLocal;
import io.netty.util.concurrent.FastThreadLocalThread;

/**
 * netty自定义的FastThreadLocal就是通过空间换时间的方式
 * 一个FastThreadLocalThread对应一个InternalThreadLocalMap，内部indexedVariables是一个Object[]数组来存储值
 * 一个FastThreadLocal对象代表一个下标，下标从1一次递增。
 * get的时候通过当前FastThreadLocal对象的index下标从，InternalThreadLocalMap的indexedVariables获取值，时间复杂度O1
 */
public class FastThreadLocalClient {
    public static void main(String[] args) throws InterruptedException {
        FastThreadLocal<Integer> fastThreadLocal = new FastThreadLocal<>();
        FastThreadLocal<Integer> fastThreadLocal1 = new FastThreadLocal<>();
        FastThreadLocal<Integer> fastThreadLocal2 = new FastThreadLocal<>();
        FastThreadLocal<Integer> fastThreadLocal3 = new FastThreadLocal<>();
        FastThreadLocal<Integer> fastThreadLocal4 = new FastThreadLocal<>();
        new FastThreadLocalThread(() -> {
            FastThreadLocal<Integer> fastThreadLocal5 = new FastThreadLocal<>();
            fastThreadLocal.set(5);
            fastThreadLocal.remove();
            fastThreadLocal1.set(6);
            fastThreadLocal2.set(7);
            fastThreadLocal3.set(8);
            fastThreadLocal4.set(9);
            fastThreadLocal4.remove();
            Integer integer1 = fastThreadLocal4.get();
            System.out.println(integer1);
            FastThreadLocal<Integer> fastThreadLocal6 = new FastThreadLocal<>();
            System.out.println("thread:" + Thread.currentThread().getName() + " 放入i=" + 5);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Integer integer = fastThreadLocal.get();
            System.out.println("thread:" + Thread.currentThread().getName() + " 取出i=" + integer);
        }).start();
        Thread.sleep(3000);
        FastThreadLocal<Integer> fastThreadLocal9 = new FastThreadLocal<>();
        fastThreadLocal9 = null;
    }
}
