package com.example.demo.user.admin.controller;

import com.example.demo.user.admin.service.CompletableFutureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

@RestController
@RequestMapping("/thread")
@PropertySource("classpath:config.properties")
public class CompletableFutureController {
    @Autowired
    private CompletableFutureService completableFutureService;

    @PostMapping("/complete")
    public Double verifyOrder() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        CompletableFuture<Double> booleanCompletableFuture = completableFutureService.verifyOrder();
//        Double aBoolean = booleanCompletableFuture.get();
//        Boolean aBoolean = booleanCompletableFuture.join();
        Double aBoolean = booleanCompletableFuture.getNow(5.0d);
        booleanCompletableFuture.thenAccept((result) -> {
            System.out.println("result is :" + result);
        });
        long end = System.currentTimeMillis();
        System.out.println("controller耗时：" + (end - start) / 1000 + "秒");
        return aBoolean;
    }

    @PostMapping("/future")
    public Double verifyOrderFuture() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        Double futureTask = completableFutureService.verifyOrderFuture();
        long end = System.currentTimeMillis();
        System.out.println("controller耗时：" + (end - start) / 1000 + "秒");
        return futureTask;
    }
}
