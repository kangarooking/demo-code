package com.example.demo.user.admin.design.strategy.impl;

import com.example.demo.user.admin.design.strategy.LuckDrawStrategy;

import org.springframework.stereotype.Component;

@Component
public class LuckPrize implements LuckDrawStrategy {
    @Override
    public String getPrizes() {
        //幸运奖根据一定的算法计算，比如是获得公司最贵商品10%的金额。
        //并且算法可能会不断改变更新。
        return "幸运奖：获得公司最贵商品10%的金额";
    }
}
