package com.example.demo.user.admin.reflect;


import io.netty.util.internal.PlatformDependent;

import java.lang.reflect.Field;

/**
 * 通过反射和unsafe类将目标对象TargetA的成员变量keySet替换为了TargetB keySet的引用
 * 简单理解就是将TargetA的keySet换成了TargetB的keySet
 */
public class ReflectClient {

    public static void main(String[] args) throws NoSuchFieldException {
        final TargetA targetA = new TargetA();
        final TargetB targetB = new TargetB();
        final MyKey b = new MyKey();
        targetB.setKeySet(b);
        Class<TargetA> targetAClass = TargetA.class;


        Field selectedKeysField = targetAClass.getDeclaredField("keySet");
        long selectedKeysFieldOffset = PlatformDependent.objectFieldOffset(selectedKeysField);
        PlatformDependent.putObject(targetA, selectedKeysFieldOffset, b);

        targetA.getKeySet().add(1);
        targetA.getKeySet().add(2);
        targetA.getKeySet().add(3);
        targetA.getKeySet().add(4);

        targetB.getKeySet().add(8);
        targetB.getKeySet().add(9);
        targetB.getKeySet().add(10);

        System.out.println("targetA的keySet=" + targetA.getKeySet());
        System.out.println("targetB的keySet=" + targetB.getKeySet());
    }
}
