package com.example.demo.user.admin.design.decorator.impl;

import com.example.demo.user.admin.design.decorator.Order;
import com.example.demo.user.admin.design.decorator.OrderDecorator;

/**
 * vip装饰者
 */
public class VipOrderDecorator implements OrderDecorator {
    @Override
    public Order decorator(Order order) {
        System.out.println("vip减去1元");
        order.setPay(order.getPrice() - 1);
        order.setPrice(order.getPrice() - 1);
        System.out.println("实付：" + order.getPay());
        return order;
    }
}
