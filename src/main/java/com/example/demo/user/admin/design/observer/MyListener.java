package com.example.demo.user.admin.design.observer;

/**
 * Listener的顶级接口，为了抽象Listener而存在
 */
public interface MyListener {
    void onEvent(DataEvent event);
}
