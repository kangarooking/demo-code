package com.example.demo.user.admin.design.adapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 新老接口的适配器
 * 有了新接口，在过渡期，为了兼容老接口的调用方（在使用spring的时候可以不修改老接口调用方的代码）而创建了适配器类
 * 本示例老接口的调用方在AdaptorClient
 */
@Component
public class InterfaceAdapter implements OldInterface {
    @Autowired
    private NewInterface newInterface;

    //使用老接口的调用方式，实际上使用的是新接口的功能
    @Override
    public void add(Integer id) {
        System.out.println("查询name");
        String name = getName();
        newInterface.save(id, name);
    }

    private String getName() {
        return "kangarooking";
    }
}
