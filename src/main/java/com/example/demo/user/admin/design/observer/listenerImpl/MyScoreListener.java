package com.example.demo.user.admin.design.observer.listenerImpl;

import com.example.demo.user.admin.design.observer.DataEvent;
import com.example.demo.user.admin.design.observer.MyListener;
import org.springframework.stereotype.Component;

/**
 * MyListener的实现类，积分监听者
 */
@Component
public class MyScoreListener implements MyListener {
    @Override
    public void onEvent(DataEvent dataEvent) {
        if (dataEvent instanceof ScoreDataEvent) {
            //...省略业务逻辑
            System.out.println("积分处理：" + dataEvent.getMsg());
        }
    }
}
