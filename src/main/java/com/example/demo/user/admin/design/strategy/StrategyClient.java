package com.example.demo.user.admin.design.strategy;

import com.example.demo.user.admin.design.strategy.entity.LuckDrawStrategyDO;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class StrategyClient {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.example.demo.user.admin.design.strategy");
        /*********************** 这里的代码可以在spring启动的过程中完成，spring有很多这样的扩展点 ***********************/
        //映射关系可以使用枚举类维护，也可以使用数据库建表维护
        //这里使用这样的方式模拟从数据库查询出来
        LuckDrawStrategyDO l1 = new LuckDrawStrategyDO(1, "firstPrize");
        LuckDrawStrategyDO l2 = new LuckDrawStrategyDO(2, "secondPrize");
        LuckDrawStrategyDO l3 = new LuckDrawStrategyDO(3, "thirdPrize");
        LuckDrawStrategyDO l4 = new LuckDrawStrategyDO(4, "luckPrize");
        //从数据库查询出来关系list
        List<LuckDrawStrategyDO> luckDrawStrategyList = Arrays.asList(l1, l2, l3, l4);
        Map<Integer, LuckDrawStrategy> map = new HashMap<>();
        //组建一个map
        luckDrawStrategyList.forEach(x -> {
            LuckDrawStrategy bean = (LuckDrawStrategy) context.getBean(x.getBeanName());
            map.put(x.getNum(), bean);
        });
        //初始化抽奖上下文
        LuckDrawContext luckDrawContext = new LuckDrawContext(map);

        /*********************** 下面是真正的抽奖逻辑代码 ***********************/
        Random random = new Random();
        int i = random.nextInt(10);
        System.out.println("数字是" + i);
        String prize = luckDrawContext.getPrize(i);
        System.out.println("恭喜" + prize);
    }
}
