package com.example.demo.user.admin.design.observer.listenerImpl;

import com.example.demo.user.admin.design.observer.DataEvent;

/**
 * 短信事件类
 */
public class SmsDataEvent extends DataEvent {
    private String phoneNum;
}
