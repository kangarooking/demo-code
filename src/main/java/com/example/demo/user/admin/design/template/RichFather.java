package com.example.demo.user.admin.design.template;

/**
 * 土豪老爹
 */
public abstract class RichFather {
    public void templateLife(){
        //养育阶段
        children();
        //上学读书阶段
        study();
        //大展宏图阶段
        cause();
        //结婚
        marry();
    }
    //定义为抽象方法，给子女自己做主
    protected abstract void marry();

    //定义为抽象方法，给子女自己做主
    protected abstract void cause();

    //养育和学习阶段统一老爹做主了
    private void study() {
        System.out.println("贵族学校，哈弗大学");
    }

    public void children(){
        System.out.println("保姆喂养，细心照料");
    }
}
