package com.example.demo.user.admin.design.chain;

import com.example.demo.user.admin.design.chain.impl.DefaultApplyChain;
import com.example.demo.user.admin.design.chain.impl.GroupLeaderHandler;
import com.example.demo.user.admin.design.chain.impl.HrHandler;
import com.example.demo.user.admin.design.chain.impl.ManagerHandler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ChainClient {
    public static void main(String[] args) {
        //该list可以从数据库中的某个表查询出来
        List<ApplyHandler<LeaveApplyMsg>> applyHandlerList = new ArrayList<>();
        ApplyHandler<LeaveApplyMsg> hr = new HrHandler(3);
        ApplyHandler<LeaveApplyMsg> gl = new GroupLeaderHandler(1);
        ApplyHandler<LeaveApplyMsg> m = new ManagerHandler(2);
        applyHandlerList.add(m);
        applyHandlerList.add(gl);
        applyHandlerList.add(hr);
        //根据order字段排序
        List<ApplyHandler<LeaveApplyMsg>> collect = applyHandlerList.stream().sorted(Comparator.comparing(ApplyHandler::getOrder)).collect(Collectors.toList());
        ApplyChain applyChain = new DefaultApplyChain();
        //循环的组装到责任链中
        for (ApplyHandler<?> applyHandler : collect) {
            applyChain.addLast(applyHandler);
        }
        //在实际场景中，上面的代码可以放到系统启动的时候初始化数据的方法中，spring有很多这样的扩展点，可以自行了解

        //这下面的代码就相当于，用户在前端选择好审批类型（对应不同的信息），然后点击申请
        //后端就触发applyChain.exc(applyMsg);这个方法开始执行整个责任链
        LeaveApplyMsg applyMsg = new LeaveApplyMsg();
        applyMsg.setMsg("apply leave");
        applyMsg.setName("lhm");
        applyMsg.setLevel(1);
        applyChain.exc(applyMsg);

    }
}
