package com.example.demo.user.admin.design.observer;

import lombok.Data;

@Data
public abstract class DataEvent {
    private String msg;
}
