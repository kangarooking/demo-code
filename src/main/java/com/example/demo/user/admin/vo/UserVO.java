package com.example.demo.user.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVO {

	private Integer id;

	private String name;

	private Integer age;

	public static void main(String[] args) {
		String str = "750001,370024,013037,000689,003567,000529,004432,004813,011329,630011,010710,290006,501057,012907,004432,008279,000536,090017,004400,150127";
		String stt = "750001,010710,008279,000536,003567,004813,011329,004400,000689,000529,004432,630011,370024,501057,012907,290006,090017";
		List<String> strings = Arrays.asList(str.split(","));
		List<String> strings2 = Arrays.asList(stt.split(","));
		Set<String> s1 = new HashSet<>();
		for (String string : strings) {
			if (s1.contains(string)){
				System.out.println(string);
			}else {
				s1.add(string);
			}
		}

		Set<String> s2 = new HashSet<>(strings2);
		s1.removeAll(s2);
		System.out.println(s1);

	}

}
