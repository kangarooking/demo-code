package com.example.demo.user.admin.design.strategy;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 抽奖上下文（抽奖的执行类）
 */
@AllArgsConstructor
@NoArgsConstructor
public class LuckDrawContext {
    private Map<Integer, LuckDrawStrategy> map;

    public String getPrize(Integer luckNum) {
        if (map.containsKey(luckNum)) {
            return map.get(luckNum).getPrizes();
        } else {
            return "谢谢惠顾";
        }
    }
}
