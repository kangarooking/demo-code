package com.example.demo.user.admin.design.chain.impl;

import com.example.demo.user.admin.design.chain.ApplyHandler;
import com.example.demo.user.admin.design.chain.LeaveApplyMsg;

public class ManagerHandler extends ApplyHandler<LeaveApplyMsg> {
    public ManagerHandler(Integer order) {
        super(order);
    }
    @Override
    protected void handle(LeaveApplyMsg applyMsg) {
        System.out.println("经理审批");
        //审批完成后继续向下申请
        apply(applyMsg);
    }
}
