package com.example.demo.user.admin.design.chain.impl;

import com.example.demo.user.admin.design.chain.ApplyHandler;
import com.example.demo.user.admin.design.chain.LeaveApplyMsg;

public class GroupLeaderHandler extends ApplyHandler<LeaveApplyMsg> {
    public GroupLeaderHandler(Integer order) {
        super(order);
    }
    @Override
    protected void handle(LeaveApplyMsg applyMsg) {
        System.out.println("小组长审批");
        //审批完成后继续向下申请
        apply(applyMsg);
    }
}
