package com.example.demo.user.admin.stream;

import com.example.demo.user.admin.vo.UserVO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamClient {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("lhm", "wss", "khz", "lhk");
        List<UserVO> list2 = new ArrayList<>();
        UserVO userVO1 = new UserVO(1,"lhm",2);
        UserVO userVO2 = new UserVO(2,"wss",3);
        list2.add(userVO1);
        list2.add(userVO2);
        List<String> collect = list2.stream().map(UserVO::getName).filter(x->x.equals("lhm")).collect(Collectors.toList());
    }
}
