package com.example.demo.user.admin.design.observer.listenerImpl;

import com.example.demo.user.admin.design.observer.DataEvent;

/**
 * 积分事件类
 */
public class ScoreDataEvent extends DataEvent {
    private Integer score;
}
