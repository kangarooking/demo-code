package com.example.demo.user.admin.design.chain.impl;

import com.example.demo.user.admin.design.chain.ApplyHandler;
import com.example.demo.user.admin.design.chain.LeaveApplyMsg;

public class HrHandler extends ApplyHandler<LeaveApplyMsg> {
    public HrHandler(Integer order) {
        super(order);
    }
    @Override
    protected void handle(LeaveApplyMsg applyMsg) {
        System.out.println("HR审批");
        //审批完成后继续向下申请
        apply(applyMsg);
    }
}
