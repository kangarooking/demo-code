package com.example.demo.user.admin.design.template.impl;

import com.example.demo.user.admin.design.template.RichFather;

/**
 * 儿子
 */
public class LandlordFoolishSon extends RichFather {
    @Override
    protected void marry() {
        System.out.println("找了一个男网红结婚了");
    }

    @Override
    protected void cause() {
        System.out.println("自己搞了一家游戏公司，虽然事业成功了，但是毁了无数青少年");
    }
}
