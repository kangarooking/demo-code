package com.example.demo.user.admin.disruptor;

import lombok.Data;

@Data
public class OrderEvent {
    private Long orderId;
}
