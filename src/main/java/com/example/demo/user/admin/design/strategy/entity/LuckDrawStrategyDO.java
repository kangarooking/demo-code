package com.example.demo.user.admin.design.strategy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * do类，也就是数据库抽奖策略表的映射对象
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LuckDrawStrategyDO {
    private Integer num;
    private String beanName;
}
