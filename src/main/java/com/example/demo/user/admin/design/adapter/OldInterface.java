package com.example.demo.user.admin.design.adapter;

/**
 * 老接口
 */
public interface OldInterface {

    //老接口抽象方法（注解标注废弃）
    @Deprecated
    public void add(Integer id);
}
