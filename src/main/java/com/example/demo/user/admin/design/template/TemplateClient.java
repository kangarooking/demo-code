package com.example.demo.user.admin.design.template;

import com.example.demo.user.admin.design.template.impl.LandlordFoolishSon;
import com.example.demo.user.admin.design.template.impl.LandlordSmartGirl;

public class TemplateClient {
    public static void main(String[] args) {
        //这样就实现了，儿子和女儿有相同的童年。长大后能自己做主自己的一些事情。
        //这就是一个模板方法。
        RichFather girl = new LandlordSmartGirl();
        RichFather son = new LandlordFoolishSon();
        girl.templateLife();
        System.out.println("---------------------------------------------");
        son.templateLife();
    }
}
