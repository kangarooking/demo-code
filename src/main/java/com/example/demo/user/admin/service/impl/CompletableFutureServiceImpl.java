package com.example.demo.user.admin.service.impl;

import com.example.demo.user.admin.config.ThreadPoolConfig;
import com.example.demo.user.admin.service.CompletableFutureService;
import com.example.demo.user.admin.thread.CustomThreadFactory;
import com.example.demo.user.admin.thread.TicketGrabCall;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

@Service
public class CompletableFutureServiceImpl implements CompletableFutureService {
    private ExecutorService executorService = ThreadPoolConfig.getThreadPool(3, 3, 10, new CustomThreadFactory());

    @Override
    public CompletableFuture<Double> verifyOrder() {
        long start = System.currentTimeMillis();
        // 验证用户能否享受这一折扣，RPC调用
        CompletableFuture<Boolean> verifyDiscountFuture = verify();

        // 获取商品单价，RPC调用
        CompletableFuture<Double> itemPriceFuture = getPrice();

        // 获取用户账号余额，限定了只能使用余额购买，RPC调用
        CompletableFuture<Double> balanceFuture = getBalance();
        CompletableFuture<Double> booleanCompletableFuture = CompletableFuture
                .allOf(verifyDiscountFuture, itemPriceFuture, balanceFuture)
                .thenApply(v -> {
                    try {
                        if (!verifyDiscountFuture.get()) {
                            // 该用户无法享受这一折扣
//                            return false;
//                            System.out.println();
                        }
                        // 用户实际应该支付的价格
                        double realPrice = itemPriceFuture.get();

                        // 用户账号余额
                        double balance = balanceFuture.get();

                        return realPrice;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return 1.0d;
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        return 1.0d;
                    }
                });
        long end = System.currentTimeMillis();
        System.out.println("service耗时：" + (end - start) / 1000 + "秒");
        return booleanCompletableFuture;

    }

    @Override
    public Double verifyOrderFuture() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        // 验证用户能否享受这一折扣，RPC调用
        Future<Boolean> verifyDiscountFuture = verifyFuture();

        // 获取商品单价，RPC调用
        Future<Double> itemPriceFuture = getPriceFuture();

        // 获取用户账号余额，限定了只能使用余额购买，RPC调用
        Future<Double> balanceFuture = getBalanceFuture();

        if (!verifyDiscountFuture.get()) {
            // 该用户无法享受这一折扣
//            return false;
        }
        // 用户实际应该支付的价格
        double realPrice = itemPriceFuture.get();
        // 用户账号余额
        double balance = balanceFuture.get();
        long end = System.currentTimeMillis();
        System.out.println("service耗时：" + (end - start) / 1000 + "秒");
        return realPrice;
    }



    private Future<Double> getBalanceFuture() {
        FutureTask<Double> futureTask = new FutureTask<>(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("获取future balance完成");
            return 10.0d;
        });
        executorService.submit(futureTask);
        return futureTask;
    }

    private Future<Double> getPriceFuture() {
        FutureTask<Double> futureTask = new FutureTask<>(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("获取future price完成");
            return 10.0d;
        });
        executorService.submit(futureTask);
        return futureTask;
    }

    private Future<Boolean> verifyFuture() {
        FutureTask<Boolean> task = new FutureTask<>(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("future 验证完成");
            return false;
        });
        executorService.submit(task);
        return task;
    }

    private CompletableFuture<Double> getBalance() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("获取balance完成");
            return 5.0d;
        });
    }

    private CompletableFuture<Double> getPrice() {

        return CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("获取price完成");
            return 6.0d;
        });
    }

    private CompletableFuture<Boolean> verify() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("验证完成");
            return false;
        });
    }

}
