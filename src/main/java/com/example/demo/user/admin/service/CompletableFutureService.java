package com.example.demo.user.admin.service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public interface CompletableFutureService {
    CompletableFuture<Double> verifyOrder();

    Double verifyOrderFuture() throws ExecutionException, InterruptedException;

}
