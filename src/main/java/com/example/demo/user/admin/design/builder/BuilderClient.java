package com.example.demo.user.admin.design.builder;

import com.example.demo.user.admin.design.decorator.Order;
import com.example.demo.user.admin.design.decorator.OrderDecorator;
import com.example.demo.user.admin.design.decorator.impl.CouponOrderDecorator;
import com.example.demo.user.admin.design.decorator.impl.VipOrderDecorator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BuilderClient {
    public static void main(String[] args) {
        /*********************** 实际项目中，可以在spring启动过程中完成这些数据的初始化 ***********************/
        OrderDecorator couponOrderDecorator = new CouponOrderDecorator();
        OrderDecorator vipOrderDecorator = new VipOrderDecorator();
        Map<Integer, OrderDecorator> map = new HashMap<>();
        map.put(1, couponOrderDecorator);
        map.put(2, vipOrderDecorator);
        OrderBuilder orderBuilder = new OrderBuilder(map);

        /*********************** 实际业务逻辑中创建订单的代码 ***********************/
        Order resultOrder = orderBuilder.orderId(1)
                .orderNo("0001")
                .price(10d)
                .pay(10d)
                .promotionTypeList(Arrays.asList(1, 2))
                .build();
    }
}
