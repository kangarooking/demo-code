package com.example.demo.user.admin.thread;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

public class CustomThreadFactory implements ThreadFactory {

    private final LongAdder threadIndex = new LongAdder();
    private final String threadNamePrefix = "ax";

    @Override
    public Thread newThread(Runnable r) {
        threadIndex.add(1);
        Thread thread = new Thread(r, threadNamePrefix + threadIndex.longValue());
        thread.setDaemon(false);
        thread.setPriority(5);
        return thread;
    }
}
