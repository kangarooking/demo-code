package com.example.demo.user.admin.design.builder;

import com.example.demo.user.admin.design.decorator.Order;
import com.example.demo.user.admin.design.decorator.OrderDecorator;

import java.util.List;
import java.util.Map;

/**
 * 订单是一个复杂对象，可以使用建造者模式来创建
 * 好处是隐藏了创建订单的细节，将创建订单和业务代码解耦
 * 使业务代码更简洁
 */
public class OrderBuilder {
    private Double price;
    private Integer orderId;
    private String orderNo;
    private Double pay;
    //优惠列表
    private List<Integer> promotionTypeList;

    public OrderBuilder price(Double price) {
        this.price = price;
        return this;
    }

    public OrderBuilder orderId(Integer orderId) {
        this.orderId = orderId;
        return this;
    }

    public OrderBuilder orderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

    public OrderBuilder pay(Double pay) {
        this.pay = pay;
        return this;
    }

    public OrderBuilder promotionTypeList(List<Integer> promotionTypeList) {
        this.promotionTypeList = promotionTypeList;
        return this;
    }

    //订单装饰者map
    private final Map<Integer, OrderDecorator> orderDecoratorMap;

    public OrderBuilder(final Map<Integer, OrderDecorator> decoratorMap) {
        this.orderDecoratorMap = decoratorMap;
    }

    //最终的创建方法
    public Order build() {
        //这里将策略模式，装饰者模式和建造者模式结合起来
        //在建造的过程中，使用策略模式来确定当前订单需要执行的优惠活动
        //使用装饰者模式来执行订单的各种优惠
        Order resultOrder = new Order(price, orderId, orderNo, pay, promotionTypeList);
        for (Map.Entry<Integer, OrderDecorator> entry : orderDecoratorMap.entrySet()) {
            Integer promotionType = entry.getKey();
            OrderDecorator orderDecorator = entry.getValue();
            if (this.promotionTypeList.contains(promotionType)) {
                resultOrder = orderDecorator.decorator(resultOrder);
            }
        }
        return resultOrder;
    }
}
