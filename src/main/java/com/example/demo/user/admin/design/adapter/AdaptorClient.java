package com.example.demo.user.admin.design.adapter;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AdaptorClient {
    public static void main(String[] args) {
        //这里的代码不用改动，只需要改动OldInterfaceImpl 和 InterfaceAdaptor的@Component注解
        //当InterfaceAdaptor有@Component注解，OldInterfaceImpl没有时，最终调用新接口的逻辑
        //当OldInterfaceImpl有@Component注解，InterfaceAdaptor没有时，就是调用老接口的逻辑

        //通过spring的AnnotationConfigApplicationContext将com.example.demo.user.admin.design路径下的所有加了spring注解的类都扫描放入spring容器
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.example.demo.user.admin.design.adaptor");
        //从spring容器中获取对应bean的实例
        OldInterface oldInterface = context.getBean(OldInterface.class);
        oldInterface.add(1);
    }
}
