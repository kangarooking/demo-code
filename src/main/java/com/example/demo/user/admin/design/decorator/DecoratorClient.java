package com.example.demo.user.admin.design.decorator;

import com.example.demo.user.admin.design.decorator.impl.CouponOrderDecorator;
import com.example.demo.user.admin.design.decorator.impl.VipOrderDecorator;

public class DecoratorClient {
    public static void main(String[] args) {
        //创建一个原始订单
        Order order = new Order(10d, 1, "0001", 10d, null);
        //创建装饰者子类对象
        OrderDecorator vipOrderDecorator = new VipOrderDecorator();
        OrderDecorator couponOrderDecorator = new CouponOrderDecorator();
        //使用vip和优惠券包装原订单对象，使其拥有vip和优惠券的优惠。
        //在某些情况下（根据具体需求来实现）还可以根据包装的顺序不同而获得不同的结果。
        Order vipCouponOrder = couponOrderDecorator.decorator(vipOrderDecorator.decorator(order));
    }
}
