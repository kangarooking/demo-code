package com.example.demo.user.admin.vo;

import lombok.Data;

@Data
public class LoginAO {
    private String name;
    private String password;
    private String sign;
}
