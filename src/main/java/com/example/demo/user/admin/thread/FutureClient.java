package com.example.demo.user.admin.thread;


import com.example.demo.user.admin.config.ThreadPoolConfig;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.FutureTask;

public class FutureClient {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = ThreadPoolConfig.getThreadPool(1, 3, 10, new CustomThreadFactory());
        FutureTask<String> task = new FutureTask<>(new TicketGrabCall());
        System.out.println("submit");
        executorService.submit(task);
        System.out.println("submit complete");
        System.out.println(task.get());
        System.out.println("shuo qu wan bi");
    }
}
