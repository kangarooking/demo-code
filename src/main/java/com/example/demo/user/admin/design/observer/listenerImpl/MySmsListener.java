package com.example.demo.user.admin.design.observer.listenerImpl;

import com.example.demo.user.admin.design.observer.DataEvent;
import com.example.demo.user.admin.design.observer.MyListener;
import org.springframework.stereotype.Component;


/**
 * MyListener的实现类，短信监听者
 */
@Component
public class MySmsListener implements MyListener {
    @Override
    public void onEvent(DataEvent dataEvent) {
        if (dataEvent instanceof SmsDataEvent) {
            //...省略短信处理逻辑
            System.out.println("短信处理");
        }
    }
}
